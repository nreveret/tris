---
author: Nicolas Revéret
title: Les Tableaux
---

# Les tableaux

## Présentation

Considérons le tableau ci-dessous présentant les fruits préférés d'un consommateur.

<table>
    <tr>
        <td style="border:1px solid; font-weight:bold;">Poire</td>
        <td style="border:1px solid; font-weight:bold;">Pomme</td>
        <td style="border:1px solid; font-weight:bold;">Orange</td>
        <td style="border:1px solid; font-weight:bold;">Kiwi</td>
    </tr>
</table>


Ce tableau a plusieurs caractéristiques :

* le fruit préféré du consommateur est dans la première colonne et c'est la « *Poire* » ;
* les types de valeurs sont uniformes : le tableau présente les fruits préférés du consommateur. Il est impossible d'y trouver la valeur « *3,59* » ;
* ce tableau comporte quatre colonnes, on ne peut pas en rajouter ni en supprimer.

En informatique, un **tableau** reprend ces caractéristiques :

* un tableau est une structure de données **linéaire**. Cela signifie qu'il contient différents éléments, rangés dans un certain ordre.
* un tableau est de **longueur fixe**. Une fois créé, il est impossible d'ajouter ou de supprimer des valeurs.
* les éléments d'un tableau sont **tous du même type**. Un tableau ne peut donc pas contenir un nombre entier suivi d'une chaîne de caractères.

Le type `#!py list` de Python permet de représenter de tels tableaux. Dans toute la suite de ce cours, **tous les tableaux seront représentés en Python par des `#!py list`**.

!!! warning "Remarque"

    En réalité les `#!py list` Python ne sont pas des tableaux au sens plein du terme : ils peuvent contenir des éléments de types variés et leur taille peut évoluer.

    Dans tout ce cours, on utilisera tout de même le type `#!py list` afin de représenter les tableaux mais l'on fera en sorte de :
    
    - ne jamais modifier la longueur d'une `#!py list`,
    - de n'y stocker que des éléments du même type.

Pour déclarer une `#!py list` on peut délimiter ses éléments avec des crochets :

```pycon
>>> nombres = [3, 8, 7]
>>> fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
```

Cette façon de faire est fastidieuse si le tableau comporte beaucoup de valeurs. Il existe d'autres façons de procéder. Retenons la suivante qui permet de créer un tableau contenant 1 000 fois la valeur `#!py None` (qui peut s'apparenter à une valeur *vide*, on ne fait que créer des cellules afin de les compléter ultérieurement) :

```pycon
>>> mille_cellules = [None] * 1000
```

Dans un tableau, chaque élément se trouve à une position précise : il y a le premier élément, le deuxième, le troisième *etc*. La position d'un élément dans le tableau est donnée par son **indice**.


!!! danger "Attention"

    Comme dans beaucoup de langages de programmation, en Python, **les indices débutent à `#!py 0`** !

    Ainsi, le premier élément est à l'indice `#!py 0`.

    | Indice     |    `#!py 0`    |    `#!py 1`    |    `#!py 2`     |   `#!py 3`    |
    | ---------- | :------------: | :------------: | :-------------: | :-----------: |
    | **Valeur** | `#!py "Poire"` | `#!py "Pomme"` | `#!py "Orange"` | `#!py "Kiwi"` |

Connaissant l'indice d'un élément dans un tableau, il est possible de récupérer sa valeur en utilisant la notation `#!py tableau[indice]`. Par exemple :

```pycon
>>> fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
>>> fruits[0]
'Poire'
>>> fruits[3]
'Kiwi'
```

??? note "Tableau et mémoire"

    Dans les langages de programmation tels que C, un tableau est stocké en machine sous forme de cellules contigües. 
    
    Connaissant l'adresse $A_0$ de la première cellule dans la mémoire, la taille $t$ d'une cellule (fixe car les éléments sont tous de même type) et l'indice $i$ d'un élément, il est aisé de calculer son adresse mémoire avec $A_i=A_0+t\times i$.

    Le fonctionnement est différent en Python car les éléments d'une `#!py list` peuvent être de types différents, ou de taille variable. Ainsi Python ne stocke pas les valeurs dans les cellules mais leurs adresses (qui, elles, sont de taille constante).

Cette instruction permet aussi de modifier une valeur :

```pycon
>>> fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
>>> fruits[1] = 'Banane'
>>> fruits
['Poire', 'Banane', 'Orange', 'Kiwi']
```

Il est possible de connaître le nombre d'éléments contenus dans un tableau en utilisant la fonction `#!py len`. Cette grandeur est la **longueur** du tableau (*length* en anglais).

```pycon
>>> fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
>>> len(fruits)
4
```

## Premiers exercices

??? question "Les meubles"

    On considère le tableau `meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']`.

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `#!py meubles[1]` vaut `#!py 'Table'`
        - [ ] `#!py meubles[1]` vaut `#!py 'Commode'`
        - [ ] `#!py meubles[4]` vaut `#!py 'Buffet'`
        - [ ] `#!py meubles[5]` vaut `#!py 'Buffet'`

    === "Solution"
        
        - :x: Les indices débutent à `#!py 0`. Donc `#!py 'Table'` est à l'indice `#!py 0`
        - :white_check_mark: `#!py meubles[1]` vaut bien `#!py 'Commode'`
        - :white_check_mark: `#!py meubles[4]` vaut bien `#!py 'Buffet'`
        - :x: Le tableau contient 5 éléments. Le dernier éléments est donc à l'indice `#!py 4`

??? question "Pointure à son pied"

    On considère désormais le tableau `pointures = [38, 43, 44, 43, 37, 42, 39, 43, 40]`.

    === "Cocher la ou les affirmations correctes"
        
        - [ ] Ce tableau est mal défini car il contient des valeurs en double
        - [ ] `#!py pointures[38]`  vaut `#!py 0`
        - [ ] `#!py pointures[3]`  est égal à `#!py pointures[8]`
        - [ ] `#!py pointures[len(pointures) - 1]`  vaut `#!py 40`

    === "Solution"
        
        - :x: Un tableau peut tout à fait contenir des valeurs en double
        - :x: Attention à ne pas confondre *indice* et *valeur*. Ici c'est `#!py pointures[0]` qui vaut `#!py 38`
        - :x: `#!py pointures[3]` est en réalité égal à `#!py pointures[7]`
        - :white_check_mark: Comme on a `#!py len(pointures)`  qui vaut `#!py 9`, le dernier élément est bien à l'indice `#!py 9 - 1`  c'est à dire `#!py 8`

??? question "Le bon meuble"

    Compléter le script ci-dessous :

    {{ IDE('pythons/indices', MODE="delayed_reveal") }}