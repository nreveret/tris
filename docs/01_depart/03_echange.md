---
author: Nicolas Revéret
title: Échanger des éléments
---

# Échanger des éléments

## Problème et solution

Lors du tri de tableaux, nous allons bien souvent échanger des valeurs. 

Par exemple, échanger `"Riri"` et `"Loulou"` dans le tableau ci-dessous :

```python
neveux = ["Riri", "Fifi", "Loulou"]
neveux[0] = neveux[2]
neveux[2] = neveux[0]
```

??? question "Les neveux en désordre"
 
    Quelle est la valeur de `#!py neveux` après l'exécution des ces instructions ?

    === "Cocher l'affirmation correcte"
            
        - [ ] `#!py ["Riri", "Fifi", "Loulou"]`
        - [ ] `#!py ["Loulou", "Fifi", "Loulou"]`
        - [ ] `#!py ["Loulou", "Fifi", "Riri"]`
        - [ ] `#!py ["Riri", "Fifi", "Riri"]`

    === "Solution"
        
        - :x: `#!py ["Riri", "Fifi", "Loulou"]`
        - :white_check_mark: `#!py ["Loulou", "Fifi", "Loulou"]`
        - :x:  `#!py ["Loulou", "Fifi", "Riri"]`
        - :x: `#!py ["Riri", "Fifi", "Riri"]`

        En effet, l'instruction `#!py neveux[0] = neveux[2]` affecte la valeur `#!py "Loulou"` à la cellule d'indice `#!py 0`. Le `#!py "Riri"` est donc « écrasé ». L'instruction `#!py neveux[2] = neveux[0]` ne fait alors que recopier le `#!py "Loulou"` dans la cellule d'indice `#!py 2`.

Nous ne pouvons donc pas procéder ainsi. À moins de pouvoir effectuer les deux instructions « en même temps » (nous verrons cela un peu plus loin).

Si l'on se représente le problème différemment, échanger les valeurs dans un tableau s'apparente à échanger le contenu des deux verres ci-dessous.

![Deux verres](images/verres.svg){ width=35% .center}

Comme on peut l'imaginer, il est nécessaire d'utiliser un troisième verre : nous devons utiliser une troisième variable.

??? question "Échanger deux valeurs"

    Compléter le script ci-dessous permettant d'échanger les valeurs d'indices `#!py 0` et `#!py 1` de la `list` `#!py fruits`.

    {{ IDE('pythons/echange') }}


## Dans une fonction

Nous allons faire un usage intensif des échanges de valeurs. Regroupons ce code dans une fonction.

??? question "La fonction `#!py echange`"

    Compléter la fonction `#!py echange` qui prend en argument un `#!py tableau` ainsi que deux entiers `#!py i` et `#!py j` et échange les éléments d'indices `#!py i` et `#!py j` dans `#!py tableau`.

    On garantit que `#!py i` et `#!py j` sont des indices valides.


    !!! danger "Attention"

        Cette fonction ne renvoie rien !

    {{ IDE('pythons/fonction_echange') }}

Attardons-nous sur ce code et observons son fonctionnement :

<iframe width="100%" height="410" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=def%20echange%28tableau,%20i,%20j%29%3A%0A%20%20%20%20temporaire%20%3D%20tableau%5Bj%5D%0A%20%20%20%20tableau%5Bj%5D%20%3D%20tableau%5Bi%5D%0A%20%20%20%20tableau%5Bi%5D%20%3D%20temporaire%0A%0Afruits%20%3D%20%5B'Poire',%20'Pomme'%5D%0Aechange%28fruits,%200,%201%29&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

Comme on peut l'observer, le tableau `#!py fruits` passé en paramètre à la fonction est directement modifiée par la fonction, quand bien même la variable utilisée dans la fonction est **différente** ; il s'agit de `tableau`.

Cette propriété est due au fait que lorsque Python transmet un tableau comme paramètre à une fonction, il transmet la **référence** du tableau et non sa **valeur**. Ainsi, le tableau modifié dans la fonction est le **même** que celui passé en paramètre.

??? warning "Complément"

    Cette particularité des tableaux ne s'applique pas à tous les types de variables. Observez le script ci-dessous :

    ```python
    def double(x):
        x = 2 * x
    
    a = 8
    double(a)
    ```

    À l'issue de ce code `a` vaut toujours `#!py 8`. En effet dans le cas des entiers, Python passe la **valeur** de l'entier en paramètre, pas sa référence.

Les tableaux (et les `#!py list`) Python ont une autre particularité importante : ce sont des objets **mutables**. Observez le fonctionnement du script ci-dessous :

<iframe width="100%" height="410" frameborder="0" src="https://pythontutor.com/iframe-embed.html#code=%23%20Cr%C3%A9ation%20d'une%20liste%20et%20d'une%20copie%0Afruits%20%3D%20%5B%22Pomme%22,%20%22Poire%22%5D%0Acopie%20%3D%20fruits%0A%0A%23%20Modification%20de%20la%20copie%0Acopie%5B0%5D%20%3D%20%22Fraise%22&codeDivHeight=400&codeDivWidth=350&cumulative=false&curInstr=0&heapPrimitives=nevernest&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false"> </iframe>

Comme on peut le voir, lors de sa création, `copie` pointe vers la même valeur que `fruits`.

On modifie ensuite la copie, certes, mais comme les deux variables **pointent vers la même `list`**, l'original a aussi été modifié.

Ce mécanisme est très important et il faudra le garder à l'esprit.

## En même temps !

Revenons au cas des deux verres observés plus haut. Afin d'échanger leur contenu, il nous a semblé indispensable d'utiliser un troisième verre... Sauf si l'on est dans la station spatiale internationale ! ([source : Nasa](https://www.nasa.gov/image-feature/astronaut-kjell-lindgren-has-fun-with-fluid-physics))

![Liquide dans l'ISS](images/iss.jpg){ width=50% .center }

Python autorise ce genre d'astuce. Il est ainsi possible d'échanger deux valeurs « en même temps ».

Pour ce faire, on utilise l'affectation multiple :

```python
nombres = [0, 1, 6, 3, 4, 5, 2]
nombres[2], nombres[6] = nombres[6], nombres[2]
```

Notez comme l'ordre des indices a été **échangé** : `#!py 2` et `#!py 6` avant l'affectation, `#!py 6` et `#!py 2` après.

Nous pouvons désormais alléger la fonction `#!py echange`.

??? question "La fonction `#!py echange_V2`"

    Modifier la fonction `#!py echange` afin qu'elle utilise l'affectation multiple.
    
    La modification du tableau se fera **en place** : la fonction ne renverra donc **pas** le tableau modifié.

    {{ IDE('pythons/fonction_echange_2') }}

??? question "La fonction `#!py renverse`"

    On souhaite écrire une fonction `#!py renverse` prenant en argument un `#!py tableau` et le renversant : le premier élément est échangé avec le dernier, le deuxième avec l'avant-dernier *etc*, s'ils existent.

    La modification s'effectuant en place, la fonction ne renverra rien.

    ??? tip "Astuce"

        On doit échanger le premier élément et le dernier, le deuxième et l'avant-dernier, le troisième et l'avant-avant-dernier, *etc.*
        jusqu'à un certain point !

    {{ IDE('pythons/renverse') }}

??? question "La fonction `#!py melange`"

    Avant de trier des tableaux, apprenons à les mélanger !
    
    L'algorithme de *Fisher-Yates* est un classique et permet de mélanger un tableau en place.
    
    Son fonctionnement est le suivant (on considère un tableau de longueur `#!py n`) :
    
    * Effectuer un parcours à rebours des indices du tableau à l'aide d'une variable `i` allant de `#!py n - 1` inclus jusqu'à `#!py 0` exclu,
        * À chaque itération, créer une variable `#!py j` et lui affecter un entier aléatoire entre `#!py 0` inclus et `#!py i` exclu,
        * Échanger les éléments d'indice `#!py i` et `#!py j` dans le tableau,
        * passer à l'itération suivante.
    
    Compléter la fonction `#!py melange` faisant subir cet algorithme à l'argument `#!py tableau`.

    ??? tip "Astuce (1)"

        La méthode `#!py randint` du module `#!py random` prend deux arguments `#!py a` et `#!py b` et renvoie un entier aléatoire entre `#!py a` et `#!py b` (inclus l'un et l'autre).
    
    ??? tip "Astuce (2)"

        Pour effectuer le parcours à rebours allant de `#!py n - 1` inclus jusqu'à `#!py 0` exclu on peut saisir `#!py for i in range(n - 1, 0, -1):`.
    

    {{ IDE('pythons/melange') }}

