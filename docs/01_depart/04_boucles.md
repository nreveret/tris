---
author: Nicolas Revéret
title: Boucles imbriquées
---

# Boucles imbriquées

## Compter des lettres dans un mot

En Python, les chaînes de caractères peuvent être parcourues comme les tableaux. Il est en particulier possible d'épeler un mot en parcourant les caractères de la chaîne :

```python
mot = "Insertion"
for lettre in mot:
    print(lettre)
```

??? question "Les points sur les « i »"

    On considère le script ci-dessous :

    ```python
    mot = "Insertion"
    compteur = 0
    for lettre in mot:
        if lettre == "i":
            compteur += 1
    ```

    Que vaut `compteur` à l'issue de ce code ?

    ??? success "Solution"

        On parcourt les lettres de la chaîne `#!py mot`. Pour chacune on vérifie si c'est un `#!py 'i'`. Si c'est le cas, la variable est incrémentée. Comme il n'y a qu'un seul `#!py 'i'` dans `#!py mot` (la casse est importante), `#!py compteur` vaut `#!py 1` à l'issue du code.

## Compter des lettres dans une phrase

Considérons désormais le problème suivant : nous avons réussi à récupérer l'ensemble des mots d'un texte dans un tableau. Chaque élément du tableau est un mot, la ponctuation a été supprimée.

Par exemple, `#!py phrase = ["Cette", "page", "traite", "des", "boucles", "imbriquées"]`.

On souhaite compter le nombre de `#!py 't'` présents dans la phrase initiale.

On peut donc :

* initialiser une variable `#!py compteur`,
* lire chaque `#!py mot` de `phrase`,
* lire chaque `#!py lettre` de chaque `#!py mot` et vérifier si c'est un `#!py 't'` ou non.

??? question "Compter les `#!py 't'`"

    Compléter le script ci-dessous permettant de compter les `#!py 't'` dans `phrase`.

    {{ IDE('pythons/compte_t')}}

## Les températures au Puy de Sancy

On fournit le tableau ci-dessous contenant le relevé des températures à 7 h du matin enregistrées au Puy de Sancy (département du Puy de Dôme) lors du mois de février 2022.

```python
fevrier = [
    [-1, 4, 1,  0, -1, -1, 0],
    [-2, 2, 3, -2, -5,  0, 3],
    [0,  4, 4,  5,  1,  1, 2],
    [0,  5, 3, -3, -3, -1, 0],
]
```

Les relevés sont regroupés en semaines : il y a 4 semaines contenant chacune 7 températures.

??? question "Compter les températures négatives dans le mois"

    Compléter le script ci-dessous permettant de compter les températures strictement négatives (il y en a 9).

    {{ IDE('pythons/temperatures_negatives')}}

??? question "Compter les températures négatives par semaine"

    On souhaite désormais compter le nombre de températures strictement négatives **par semaine**. Les résultats seront stockés dans un tableau `#!py effectifs` contenant initialement quatre 0.
    
    Compléter le script ci-dessous permettant de compter les températures strictement négatives par semaine.

    ??? tip "Astuce"

        Il faut garder trace de l'indice de la semaine étudiée. Par contre, il ne sert à rien de garder trace de l'indice du jour.

    {{ IDE('pythons/temperatures_negatives_semaine')}}

## Compter les inversions

Nous avons vu précédemment une méthode permettant de mélanger un tableau. Il est possible de mesurer le « *désordre* » d'un tableau en comptant ses *inversions*.

Une inversion est un couple d'indices `#!py i` et `#!py j` (`#!py i` strictement **inférieur** à `#!py j`) tel que `#!py tableau[i]` est strictement **supérieur** à `#!py tableau[j]`.

Par exemple, le tableau `#!py [3, 8, 1]` compte deux inversions : les couples d'indices `#!py (0, 2)` et `#!py (1, 2)`.

Il est possible de compter les inversions dans un tableau en imbriquant deux boucles :

* la boucle principale parcourt tous les indices `#!py i` jusqu'à l'avant-dernier inclus,
* la boucle imbriquée parcourt les indices allant de `#!py i + 1` jusqu'à la fin du tableau.

Au sein de ces deux boucles, on compare les valeurs des éléments correspondants.

??? question "La fonction `#!py inversions`"

    Compléter la fonction `#!py inversions` ci-dessous permettant de compter les inversions présentes dans `#!py tableau`.

    {{ IDE('pythons/inversions')}}

??? question "La fonction `#!py plus_petits`"

    Compléter la fonction `plus_petits` ci-dessous permettant de compter le nombre d'inversions concernant chaque élément, c'est à dire, le nombre de valeurs situées après cet élément et qui lui sont strictement inférieures.

    Par exemple pour le tableau `#!py [3, 8, 1]` on obtiendra `#!py [1, 1, 0]` car :

    * `#!py 3` possède un seul élément qui lui est strictement inférieur à sa droite,
    * `#!py 8` possède un seul élément qui lui est strictement inférieur à sa droite,
    * `#!py 1` ne possède aucun élément qui lui est strictement inférieur à sa droite.

    {{ IDE('pythons/plus_petits')}}
    