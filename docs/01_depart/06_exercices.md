---
author: Nicolas Revéret
title: Exercices sur les tableaux
---

# Exercices sur les tableaux

??? question "Parcours sur les indices ou sur les valeurs ?"

    On considère un `#!py tableau` **non vide** contenant des valeurs quelconques. Indiquer dans chaque cas le bon type de parcours à effectuer.

    * On souhaite déterminer **l'indice de la valeur maximale**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :white_check_mark: Parcours sur les indices
            - :x: Parcours sur les valeurs

    * On souhaite calculer **la somme des valeurs**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :x: Parcours sur les indices
            - :white_check_mark: Parcours sur les valeurs

    * On souhaite **créer un nouveau tableau ne contenant que les valeurs de la première moitié de `#!py tableau`**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :white_check_mark: Parcours sur les indices
            - :x: Parcours sur les valeurs

    * On souhaite **déterminer les deux *extrema* (minimum et maximum)**.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :x: Parcours sur les indices
            - :white_check_mark: Parcours sur les valeurs

    * On souhaite **élever au carré toutes les valeurs du `#!py tableau`** en écrivant les nouvelles valeurs dans le même `#!py tableau`. Par exemple `#!py [2, 3, 4]` deviendrait `#!py [4, 9, 16]`.

        === "Cocher la bonne réponse"
            
            - [ ] Parcours sur les indices
            - [ ] Parcours sur les valeurs

        === "Solution"
            
            - :white_check_mark: Parcours sur les indices
            - :white_check_mark: Parcours sur les valeurs avec une liste en compréhension : `#!py tableau = [x * x for x in tableau]`

??? question "Recherche d'indice - non guidé"

    Il s'agit de déterminer l'indice de la plus petite valeur dans un tableau non-vide.

    [Indice du minimum d'un tableau](https://codex.forge.apps.education.fr/exercices/ind_min/){ .md-button target="_blank" rel="noopener" }

??? question "Recherche de valeur - non guidé"

    La recherche de la valeur maximale dans un tableau. Classique.
    
    [Maximum](https://codex.forge.apps.education.fr/exercices/maximum_nombres/){ .md-button target="_blank" rel="noopener" }

??? question "Lecture dans un tableau - non guidé"

    On donne les altitudes des différentes étapes d'une course en montagne. On demande quel est le dénivelé positif total.

    [Dénivelé positif](https://codex.forge.apps.education.fr/exercices/denivele_positif/){ .md-button target="_blank" rel="noopener" }

??? question "Comparaison d'éléments consécutifs - non guidé"

    Le tableau fourni est-il trié ?

    [Est trié ?](https://codex.forge.apps.education.fr/exercices/est_trie/){ .md-button target="_blank" rel="noopener" }

??? question "Modification d'un tableau - non guidé"

    On se donne un tableau, une valeur cible et une valeur de remplacement et il faut parcourir le tableau et remplacer la cible par la nouvelle valeur.

    [Remplacer une valeur](https://codex.forge.apps.education.fr/exercices/remplacer/){ .md-button target="_blank" rel="noopener" }
    
??? question "Recherche de maxima relatifs - non guidé"

    Combien de bâtiments sont éclairés par le soleil couchant. Le sujet est original mais l'algorithme très classique.

    [Soleil couchant](https://codex.forge.apps.education.fr/exercices/soleil_couchant/){ .md-button target="_blank" rel="noopener" }


