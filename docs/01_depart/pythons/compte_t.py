# --- PYODIDE:code --- #
phrase = ["Cette", "page", "traîte", "des", "boucles", "imbriquées"]

compteur = ...
...  # plusieurs lignes possibles
# --- PYODIDE:corr --- #
phrase = ["Cette", "page", "traîte", "des", "boucles", "imbriquées"]

compteur = 0
for mot in phrase:
    for lettre in mot:
        if lettre == "t":
            compteur += 1
# --- PYODIDE:secrets --- #
assert compteur == 4, f"Erreur, cotre code doit compter 4 « t »"
