# --- PYODIDE:code --- #
def derniere_occurrence(tableau, cible):
    ...


# --- PYODIDE:corr --- #
def derniere_occurrence(tableau, cible):
    resultat = None
    for i in range(len(tableau)):
        if tableau[i] == cible:
            resultat = i
    return resultat
# --- PYODIDE:tests --- #
nombres = [3, 8, 5, 4, 3, 8]
assert derniere_occurrence(nombres, 3) == 4, "Erreur en cherchant 3"
# --- PYODIDE:secrets --- #
nombres = [3, 8, 5, 4, 3, 8]
assert derniere_occurrence(nombres, 8) == 5, "Erreur en cherchant 8"
assert derniere_occurrence(nombres, 5) == 2, "Erreur en cherchant 5"
assert derniere_occurrence(nombres, 0) is None, "Erreur en cherchant 0"
