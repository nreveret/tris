Une solution alternative et plus astucieuse consiste à parcourir le tableau en partant de la fin. Ainsi, dès que l'on rencontre la `#!py cible` on peut renvoyer l'indice correspondant :

```python
def derniere_occurrence(tableau, cible):
    for i in range(len(tableau) - 1, -1, -1):
        if tableau[i] == cible:
            return i
    return None
```