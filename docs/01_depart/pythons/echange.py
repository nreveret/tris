# --- PYODIDE:code --- #
fruits = ['Poire', 'Pomme']

temporaire = ...
...
...

# --- PYODIDE:corr --- #
fruits = ['Poire', 'Pomme']

temporaire = fruits[1]
fruits[1] = fruits[0]
fruits[0] = temporaire
# --- PYODIDE:secrets --- #
assert fruits[0] == "Pomme", "!!! Erreur !!!"
assert fruits[1] == "Poire", "!!! Erreur !!!"
