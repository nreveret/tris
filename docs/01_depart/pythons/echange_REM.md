On pourrait aussi affecter `#!py fruits[0]` à `#!py temporaire` :

```python
temporaire = fruits[0]
fruits[0] = fruits[1]
fruits[1] = temporaire
```