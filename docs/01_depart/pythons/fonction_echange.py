# --- PYODIDE:code --- #
def echange(tableau, i, j):
    ...


# --- PYODIDE:corr --- #
def echange(tableau, i, j):
    temporaire = tableau[j]
    tableau[j] = tableau[i]
    tableau[i] = temporaire
# --- PYODIDE:tests --- #
fruits = ['Poire', 'Pomme']
echange(fruits, 0, 1)
assert fruits[0] == "Pomme"
assert fruits[1] == "Poire"
# --- PYODIDE:secrets --- #
legumes = ['Carotte', 'Aubergine', 'Concombre']
echange(legumes, 1, 2)
assert legumes[0] == "Carotte", f"Erreur en échangeant des valeurs dans {legumes = }"
assert legumes[1] == "Concombre", f"Erreur en échangeant des valeurs dans {legumes = }"
assert legumes[2] == "Aubergine", f"Erreur en échangeant des valeurs dans {legumes = }"