# --- PYODIDE:code --- #
def echange_V2(tableau, i, j):
    ...


# --- PYODIDE:corr --- #
def echange_V2(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]
# --- PYODIDE:tests --- #
fruits = ['Poire', 'Pomme']
echange_V2(fruits, 0, 1)
assert fruits[0] == "Pomme"
assert fruits[1] == "Poire"
# --- PYODIDE:secrets --- #
legumes = ['Carotte', 'Aubergine', 'Concombre']
echange_V2(legumes, 1, 2)
assert legumes[0] == "Carotte", f"Erreur en échangeant des valeurs dans {legumes = }"
assert legumes[1] == "Concombre", f"Erreur en échangeant des valeurs dans {legumes = }"
assert legumes[2] == "Aubergine", f"Erreur en échangeant des valeurs dans {legumes = }"