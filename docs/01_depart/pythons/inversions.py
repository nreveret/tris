# --- PYODIDE:code --- #
def inversions(tableau):
    ...


# --- PYODIDE:corr --- #
def inversions(tableau):
    total = 0
    for i in range(len(tableau) - 1):
        for j in range(i, len(tableau)):
            if tableau[i] > tableau[j]:
                total += 1
    return total
# --- PYODIDE:tests --- #
tableau_1 = [3, 8, 1]
assert inversions(tableau_1) == 2, "Erreur avec tableau_1"

# --- PYODIDE:secrets --- #
tableau_2 = [30, 10, 23, 18, 23, 27, 20, 15, 21, 26, 24, 30, 15, 19]
attendu = 44
assert inversions(tableau_2) == attendu, "Erreur avec un autre tableau"
