# --- PYODIDE:code --- #
from random import randint

def echange(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]


def melange(tableau):
    n = len(tableau)
    ...


# --- PYODIDE:corr --- #
from random import randint

def echange(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]
    

def melange(tableau):
    for i in range(len(tableau) - 1, 0, -1):
        j = randint(0, i)
        echange(tableau, i, j)
# --- PYODIDE:tests --- #
# Pas de tests car le résultat est "hasardeux" ;-)
# --- PYODIDE:secrets --- #
tableau = [0, 1, 2, 3, 4]
melange(tableau)
print(tableau)


