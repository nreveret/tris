# --- PYODIDE:code --- #
def plus_petits(tableau):
    n = len(tableau)
    bilan = [0] * n
    ...


# --- PYODIDE:corr --- #
def plus_petits(tableau):
    n = len(tableau)
    bilan = [0] * n
    for i in range(len(tableau) - 1):
        for j in range(i + 1, len(tableau)):
            if tableau[i] > tableau[j]:
                bilan[i] += 1
    return bilan


# --- PYODIDE:tests --- #
tableau_1 = [3, 8, 1]
assert plus_petits(tableau_1) == [1, 1, 0], "Erreur avec tableau_1"

# --- PYODIDE:secrets --- #
tableau_2 = [30, 10, 23, 18, 23, 27, 20, 15, 21]
attendu = [8, 0, 4, 1, 3, 3, 1, 0, 0]
assert plus_petits(tableau_2) == attendu, "Erreur avec un autre tableau"
