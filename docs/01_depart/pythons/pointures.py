# --- PYODIDE:code --- #
pointures = [43, 39, 43, 41, 44, 37, 39, 43, 38, 37, 39, 39, 41, 43, 44]

compteur = ...
for ... in ...:
    if ... == 41:
        ... = ...

# --- PYODIDE:corr --- #
pointures = [43, 39, 43, 41, 44, 37, 39, 43, 38, 37, 39, 39, 41, 43, 44]

compteur = 0
for p in pointures:
    if p == 41:
        compteur += 1
# --- PYODIDE:secrets --- #
assert compteur == 2, "Le compteur devrait valoir 2"