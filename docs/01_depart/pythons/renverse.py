# --- PYODIDE:code --- #
def echange(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]


def renverse(tableau):
    ...


# --- PYODIDE:corr --- #
def echange(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]
    
    
def renverse(tableau):
    i = 0
    j = len(tableau) - 1
    while i < j:
        echange(tableau, i, j)
        i += 1
        j -= 1


# --- PYODIDE:tests --- #
voyelles = ["a", "e", "i", "o", "u", "y"]
renverse(voyelles)
assert voyelles == ["y", "u", "o", "i", "e", "a"]
# --- PYODIDE:secrets --- #
premiers = [2, 3, 5, 7]
renverse(premiers)
assert premiers == [7, 5, 3, 2], "Erreur en renversant la liste [2, 3, 5, 7]"
