Il s'agit d'échanger les éléments du début du tableau avec ceux de la fin. On utilise deux indices `i` et `j`, l'un progressant du début vers la fin du tableau, l'autre de la fin vers le début.

À chaque étape du parcours, on échange les éléments d'indices `i` et `j`.

Lorsque `i` devient supérieur ou égal à `j`, cela signifie que l'on a dépassé le milieu du tableau : tous les échanges souhaités ont eu lieu.