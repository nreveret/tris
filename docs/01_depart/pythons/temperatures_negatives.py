# --- PYODIDE:code --- #
fevrier = [
    [-1, 4, 1,  0, -1, -1, 0],
    [-2, 2, 3, -2, -5,  0, 3],
    [0,  4, 4,  5,  1,  1, 2],
    [0,  5, 3, -3, -3, -1, 0]
]


nb_negatives = 0
...


# --- PYODIDE:corr --- #
fevrier = [
    [-1, 4, 1,  0, -1, -1, 0],
    [-2, 2, 3, -2, -5,  0, 3],
    [0,  4, 4,  5,  1,  1, 2],
    [0,  5, 3, -3, -3, -1, 0]
]


negatives = 0
for semaine in fevrier:
    for temperature in semaine:
        if temperature < 0:
            negatives += 1
# --- PYODIDE:secrets --- #
assert negatives == 9, "Erreur, réessayez !"
