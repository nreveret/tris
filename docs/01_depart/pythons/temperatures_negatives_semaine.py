# --- PYODIDE:code --- #
fevrier = [
    [-1, 4, 1,  0, -1, -1, 0],
    [-2, 2, 3, -2, -5,  0, 3],
    [0,  4, 4,  5,  1,  1, 2],
    [0,  5, 3, -3, -3, -1, 0]
]

effectifs = [0] * 4
...

# --- PYODIDE:corr --- #
fevrier = [
    [-1, 4, 1,  0, -1, -1, 0],
    [-2, 2, 3, -2, -5,  0, 3],
    [0,  4, 4,  5,  1,  1, 2],
    [0,  5, 3, -3, -3, -1, 0]
]

effectifs = [0] * 4
for i in range(len(fevrier)):
    semaine = fevrier[i]
    for temperature in semaine:
        if temperature < 0:
            effectifs[i] += 1
# --- PYODIDE:secrets --- #
assert effectifs == [3, 3, 0, 3], "Erreur, réessayez !"
