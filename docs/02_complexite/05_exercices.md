---
author: Nicolas Revéret
title: Exercices sur les coûts des algorithmes
---

# Exercices sur les coûts des algorithmes

??? question "Lectures dans deux tableaux (1) - non guidé"

    Les éléments d'un tableau sont-ils tous différents ?

    [Éléments tous différents](https://codex.forge.apps.education.fr/exercices/tous_differents/){ .md-button target="_blank" rel="noopener" }

??? question "Lectures dans deux tableaux (2) - non guidé"

    On se donne deux tableaux de même longueur. Il s'agit de lister les différences entre les éléments de mêmes indices.
    
    [Différences entre deux tableaux](https://codex.forge.apps.education.fr/exercices/liste_differences/){ .md-button target="_blank" rel="noopener" }

