# --- PYODIDE:env --- #
def lineaire_bis(tableau, x):
    nb_iterations = 0
    for i in range(len(tableau)):
        nb_iterations += 1
        if tableau[i] == x:
            return i
    return nb_iterations
# --- PYODIDE:code --- #
# 1 itération
tableau = [8, 7, 2, 10, 3, 1, 6, 5, 9, 5, 4]
x = 5
assert lineaire_bis(tableau, x) == 1

# 2 itérations
tableau = [8, 7, 2, 10, 3, 1, 6, 5, 9, 5, 4]
x = 5
assert lineaire_bis(tableau, x) == 2

# 5 itérations
tableau = [8, 7, 2, 10, 3, 1, 6, 5, 9, 5, 4]
x = 5
assert lineaire_bis(tableau, x) == 5

# 5 itérations dans un tableau de 6 valeurs
tableau = [...]
x = 5
assert lineaire_bis(tableau, x) == 5
assert len(tableau) == 5

# 5 itérations dans un tableau contenant 2 fois la valeur cherchée
tableau = [...]
x = 5
assert lineaire_bis(tableau, x) == 5
assert len([y for y in tableau if y == x]) == 2

# 6 itérations dans un tableau de 6 valeurs sans que x ne soit présent dans tableau
tableau = [...]
x = 5
assert lineaire_bis(tableau, x) == 6
assert len(tableau) == 6
assert x not in tableau
