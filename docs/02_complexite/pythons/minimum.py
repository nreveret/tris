# --- PYODIDE:code --- #
def indice_du_minimum(tableau):
    ...


# --- PYODIDE:corr --- #
def indice_du_minimum(tableau):
    i_mini = 0
    for i in range(1, len(tableau)):
        if tableau[i] < tableau[i_mini]:
            i_mini = i
    return i_mini


# --- PYODIDE:secrets --- #
assert indice_du_minimum([3, 8, 5]) == 0, "Erreur avec [3, 8, 5]"
# --- PYODIDE:tests --- #
assert indice_du_minimum([8, 5]) == 1, "Erreur avec [8, 5]"
assert indice_du_minimum([8]) == 0, "Erreur avec [8]"
