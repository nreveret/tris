# --- PYODIDE:code --- #
def lineaire(tableau, x):
    ...


# --- PYODIDE:corr --- #
def lineaire(tableau, x):
    for i in range(len(tableau)):
        if tableau[i] == x:
            return i
    return None
# --- PYODIDE:tests --- #
tableau = [8, 7, 2, 10, 3, 1, 6, 5, 9, 5, 4]
assert lineaire(tableau, 5) == 7, "Erreur en cherchant 5"
# --- PYODIDE:secrets --- #
assert lineaire(tableau, 0) is None, "Erreur en cherchant 0"

tableau = [17]
attendu = 0
assert lineaire(tableau, 17) == attendu, "Erreur en cherchant dans un autre tableau"
assert lineaire(tableau, 18) is None, "Erreur en cherchant dans un autre tableau"
