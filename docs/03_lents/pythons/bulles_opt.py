# --- PYODIDE:code --- #
def echange(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]


def tri_bulles_opt(tableau):
    ...


# --- PYODIDE:corr --- #
def echange(tableau, i, j):
    tableau[i], tableau[j] = tableau[j], tableau[i]

def tri_bulles_opt(tableau):
    for i in range(len(tableau) - 1, 0, -1):
        tableau_trie = True
        for j in range(0, i):
            if tableau[j] > tableau[j + 1]:
                echange(tableau, j, j + 1)
                tableau_trie = False
        if tableau_trie:
            break  # on quitte de façon prématurée
# --- PYODIDE:tests --- #
tableau_0 = [3, 1, 2]
tri_bulles_opt(tableau_0)
assert tableau_0 == [1, 2, 3], "Erreur avec [3, 1, 2]"

# --- PYODIDE:secrets --- #
tableau_1 = [1, 2, 3, 4]
tri_bulles_opt(tableau_1)
assert tableau_1 == [1, 2, 3, 4], "Erreur avec [1, 2, 3, 4]"

tableau_2 = [-2, -5]
tri_bulles_opt(tableau_2)
assert tableau_2 == [-5, -2], "Erreur avec des valeurs négatives"

tableau_3 = []
tri_bulles_opt(tableau_3)
assert tableau_3 == [], "Erreur avec un tableau vide"
