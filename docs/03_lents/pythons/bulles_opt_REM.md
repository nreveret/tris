Désormais, face à un tableau initialement trié dans l'ordre croissant, l'algorithme n'effectuera qu'une seule itération. Le coût sera alors **linéaire**.
