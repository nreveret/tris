# --- PYODIDE:code --- #
def tri_insertion_decr(tableau):
    ...


# --- PYODIDE:corr --- #
def tri_insertion_decr(tableau):
    for i in range(1, len(tableau)):
        valeur_a_inserer = tableau[i]
        j = i
        while j > 0 and tableau[j - 1] < valeur_a_inserer:
            tableau[j] = tableau[j - 1]
            j = j - 1
        tableau[j] = valeur_a_inserer
# --- PYODIDE:tests --- #
tableau_0 = [3, 1, 2]
tri_insertion_decr(tableau_0)
assert tableau_0 == [3, 2, 1], "Erreur avec [3, 1, 2]"

# --- PYODIDE:secrets --- #
tableau_1 = [1, 2, 3, 4]
tri_insertion_decr(tableau_1)
assert tableau_1 == [4, 3, 2, 1], "Erreur avec [1, 2, 3, 4]"

tableau_2 = [-2, -5]
tri_insertion_decr(tableau_2)
assert tableau_2 == [-2, -5], "Erreur avec des valeurs négatives"

tableau_3 = []
tri_insertion_decr(tableau_3)
assert tableau_3 == [], "Erreur avec un tableau vide"
