# --- PYODIDE:code --- #
def indice_minimum_depuis(tableau, i):
    i_mini = i
    ...

# --- PYODIDE:corr --- #
def indice_minimum_depuis(tableau, i):
    i_mini = i
    for j in range(i + 1, len(tableau)):
        if tableau[j] < tableau[i_mini]:
            i_mini = j
    return i_mini
# --- PYODIDE:tests --- #
assert indice_minimum_depuis([3, 8, 1, 5, 4], 0) == 2, "Erreur en partant de l'indice 0"
# --- PYODIDE:secrets --- #
assert indice_minimum_depuis([3, 8, 1, 5, 4], 1) == 2, "Erreur en partant de l'indice 1"
assert indice_minimum_depuis([3, 8, 1, 5, 4], 2) == 2, "Erreur en partant de l'indice 2"
assert indice_minimum_depuis([3, 8, 1, 5, 4], 4) == 4, "Erreur en partant de l'indice 4"
