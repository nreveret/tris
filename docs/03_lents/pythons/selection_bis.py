# --- PYODIDE:code --- #
def tri_selection(tableau):
    ...


# --- PYODIDE:corr --- #
def tri_selection(tableau):
    for i in range(len(tableau) - 1):
        i_mini = i
        for j in range(i + 1, len(tableau)):
            if tableau[j] < tableau[i_mini]:
                i_mini = j
        tableau[i], tableau[i_mini] = tableau[i_mini], tableau[i]
# --- PYODIDE:tests --- #
tableau_0 = [3, 1, 2]
tri_selection(tableau_0)
assert tableau_0 == [1, 2, 3], "Erreur avec [3, 1, 2]"

# --- PYODIDE:secrets --- #
tableau_1 = [1, 2, 3, 4]
tri_selection(tableau_1)
assert tableau_1 == [1, 2, 3, 4], "Erreur avec [1, 2, 3, 4]"

tableau_2 = [-2, -5]
tri_selection(tableau_2)
assert tableau_2 == [-5, -2], "Erreur avec des valeurs négatives"

tableau_4 = []
tri_selection(tableau_4)
assert tableau_4 == [], "Erreur avec un tableau vide"

