# --- PYODIDE:code --- #
def indice_prochain_depuis(tableau, i):
    ...


def tri_eleves(tableau):
    ...


# --- PYODIDE:corr --- #
def indice_prochain_depuis(tableau, i):
    i_maxi = i
    for j in range(i + 1, len(tableau)):
        if tableau[j][0] > tableau[i_maxi][0]:
            i_maxi = j
        elif tableau[j][0] == tableau[i_maxi][0]:
            if tableau[j][1] < tableau[i_maxi][1]:
                i_maxi = j
    return i_maxi


def tri_eleves(tableau):
    for i in range(len(tableau) - 1):
        i_prochain = indice_prochain_depuis(tableau, i)
        tableau[i], tableau[i_prochain] = tableau[i_prochain], tableau[i]
# --- PYODIDE:tests --- #
eleves = [
    (4, "Targeur Samia"),
    (5, "Blennie Aymeric"),
    (5, "Alose Tom"),
    (6, "Targeur Samir"),
]
tri_eleves(eleves)
# --- PYODIDE:secrets --- #
assert eleves == [
    (6, "Targeur Samir"),
    (5, "Alose Tom"),
    (5, "Blennie Aymeric"),
    (4, "Targeur Samia"),
]
print("Bravo !")
