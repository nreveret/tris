# --- PYODIDE:code --- #
def tri_selection_decr(tableau):
    ...


# --- PYODIDE:corr --- #
def tri_selection_decr(tableau):
    for i in range(len(tableau) - 1):
        i_maxi = i
        for j in range(i + 1, len(tableau)):
            if tableau[j] > tableau[i_maxi]:
                i_maxi = j
        tableau[i], tableau[i_maxi] = tableau[i_maxi], tableau[i]
# --- PYODIDE:tests --- #
tableau = [3, 1, 2]
tri_selection_decr(tableau)
assert tableau == [3, 2, 1], "Erreur avec [3, 1, 2]"

# --- PYODIDE:secrets --- #
tableau = [1, 2, 3, 4]
tri_selection_decr(tableau)
assert tableau == [4, 3, 2, 1], "Erreur avec [1, 2, 3, 4]"

tableau = [-2, -5]
tri_selection_decr(tableau)
assert tableau == [-2, -5], "Erreur avec des valeurs négatives"

tableau = []
tri_selection_decr(tableau)
assert tableau == [], "Erreur avec un tableau vide"
