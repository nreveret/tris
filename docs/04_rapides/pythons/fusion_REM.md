L'approche précédente est classique. Il est néanmoins possible de la réécrire à l'aide d'une unique boucle bornée (au lieu de trois boucles non bornées).

```python
def fusion(gauche, droite):
    """
    Fusionne gauche et droite
    Les tableaux initiaux sont triés
    Le tableau résultat l'est aussi
    """
    taille_gauche = len(gauche)
    taille_droite = len(droite)
    nouveau = [None] * (taille_gauche + taille_droite)

    i_nouveau = 0
    i_gauche = 0
    i_droite = 0

    for i_nouveau in range(taille_gauche + taille_droite):
        if i_gauche < taille_gauche and (i_droite >= taille_droite or gauche[i_gauche] <= droite[i_droite]):
            nouveau[i_nouveau] = gauche[i_gauche]
            i_gauche += 1
        else:
            nouveau[i_nouveau] = droite[i_droite]
            i_droite += 1
    
    return nouveau
```