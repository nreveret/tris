# --- PYODIDE:code --- #
def sous_tableau(tableau, debut, fin):
    return [tableau[valeur] for valeur in range(debut, fin)]


def inversions(tableau, debut, fin):
    ...


# --- PYODIDE:corr --- #
def sous_tableau(tableau, debut, fin):
    nouveau = [None] * (fin - debut)
    for i in range(debut, fin):
        nouveau[i - debut] = tableau[i]
    return nouveau


def inversions(tableau, debut, fin):
    # Cas de base, tableau de longueur 1 ou moins
    if (fin - debut) < 2:
        return 0

    milieu = (fin + debut) // 2

    nb_inversions = 0
    # Tris récursifs des parties gauche et droite
    nb_inversions += inversions(tableau, debut, milieu)
    nb_inversions += inversions(tableau, milieu, fin)

    # Fusion
    gauche = sous_tableau(tableau, debut, milieu)
    droite = sous_tableau(tableau, milieu, fin)

    i_gauche = 0
    i_droite = 0
    i_tableau = debut

    # Il reste des éléments à gauche ET à droite
    while i_gauche < len(gauche) and i_droite < len(droite):
        if gauche[i_gauche] <= droite[i_droite]:
            tableau[i_tableau] = gauche[i_gauche]
            i_gauche += 1
        else:
            tableau[i_tableau] = droite[i_droite]
            i_droite += 1
            nb_inversions += len(gauche) - i_gauche
        i_tableau += 1

    # Il ne reste des éléments QUE à gauche
    while i_gauche < len(gauche):
        tableau[i_tableau] = gauche[i_gauche]
        i_gauche += 1
        i_tableau += 1

    # Il ne reste des éléments QUE à droite
    while i_droite < len(droite):
        tableau[i_tableau] = droite[i_droite]
        i_droite += 1
        i_tableau += 1

    return nb_inversions
# --- PYODIDE:tests --- #
tableau_1 = [3, 8, 1]
assert inversions(tableau_1, 0, len(tableau_1)) == 2, "Erreur avec tableau_1"

# --- PYODIDE:secrets --- #
tableau_2 = [30, 10, 23, 18, 23, 27, 20, 15, 21, 26, 24, 30, 15, 19]
assert inversions(tableau_2, 0, len(tableau_2)) == 44, "Erreur avec tableau_2"
