# --- PYODIDE:code --- #
def fusion(gauche, droite, sentinelle):
    taille_gauche = len(gauche)
    taille_droite = len(droite)

    gauche_etendu = ...
    droite_etendu = ...

    nouveau = [None] * (...)

    i_nouveau = 0
    i_gauche = 0
    i_droite = 0

    # Il reste des éléments à gauche ET à droite
    while ...:
        if gauche_etendu[i_gauche] <= droite_etendu[i_droite]:
            nouveau[i_nouveau] = gauche_etendu[i_gauche]
            i_gauche += 1
        else:
            nouveau[i_nouveau] = droite_etendu[i_droite]
            i_droite += 1
        i_nouveau += 1

    return nouveau


def tri_fusion(tableau, sentinelle):
    taille = len(tableau)
    # Cas de base, tableau de longueur 1 ou moins
    if taille < 2:
        return tableau

    milieu = taille // 2

    # Tris récursifs des parties gauche et droite
    gauche = tri_fusion(..., ...)
    droite = tri_fusion(..., ...)

    return fusion(gauche, droite, ...)


# --- PYODIDE:corr --- #
def fusion(gauche, droite, sentinelle):
    taille_gauche = len(gauche)
    taille_droite = len(droite)

    gauche_etendu = gauche + [sentinelle]

    droite_etendu = droite + [sentinelle]

    nouveau = [None] * (taille_gauche + taille_droite)

    i_nouveau = 0
    i_gauche = 0
    i_droite = 0

    # Il reste des éléments à gauche ET à droite
    while i_gauche != taille_gauche or i_droite != taille_droite:
        if gauche_etendu[i_gauche] <= droite_etendu[i_droite]:
            nouveau[i_nouveau] = gauche_etendu[i_gauche]
            i_gauche += 1
        else:
            nouveau[i_nouveau] = droite_etendu[i_droite]
            i_droite += 1
        i_nouveau += 1

    return nouveau


def tri_fusion(tableau, sentinelle):
    taille = len(tableau)
    # Cas de base, tableau de longueur 1 ou moins
    if taille < 2:
        return tableau

    milieu = taille // 2

    # Tris récursifs des parties gauche et droite
    gauche = tri_fusion(tableau[:milieu], sentinelle)
    droite = tri_fusion(tableau[milieu:], sentinelle)

    return fusion(gauche, droite, sentinelle)
# --- PYODIDE:tests --- #
tableau = [3, 1, 2]
sentinelle = max(tableau) + 1
assert tri_fusion(tableau, sentinelle) == [1, 2, 3], "Erreur avec [3, 1, 2]"

# --- PYODIDE:secrets --- #
tableau = [1, 2, 3, 4]
sentinelle = max(tableau) + 1
assert tri_fusion(tableau, sentinelle) == [1, 2, 3, 4], "Erreur avec [1, 2, 3, 4]"

tableau = [-2, -5]
sentinelle = max(tableau) + 1
assert tri_fusion(tableau, sentinelle) == [-5, -2], "Erreur avec des valeurs négatives"
