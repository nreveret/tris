Les appels de la fonction de tri fusion doivent donc désormais contenir la valeur de la sentinelle :

```python
tableau = [307, 198, 203, -500]
resultat = tri_fusion(tableau, max(tableau) + 1)
```

Python offre aussi une solution alternative à l'aide de l'objet `#!py float('inf')`. Ce nombre flottant est en effet strictement supérieur à n'importe quel nombre entier `#!py int` manipulé par Python. Il est possible d'utiliser ce valeur comme sentinelle. On alors simplement passer une valeur par défaut au paramètre `#!py sentinelle` :

```python
def tri_fusion(tableau, sentinelle=float('inf')):
    pass
```