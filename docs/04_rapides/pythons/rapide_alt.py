# --- PYODIDE:code --- #
def tri_rapide_hoare(tableau, debut, fin):
    # Cas de base
    if fin - debut < 2:
        return

    pivot = tableau[debut]
    gauche = debut
    droite = fin - 1

    while ...:
        ...

    # Échange
    ...
    # Appels récursifs
    tri_rapide_hoare(tableau, debut, droite)
    tri_rapide_hoare(tableau, droite + 1, fin)


# --- PYODIDE:corr --- #
def tri_rapide_hoare(tableau, debut, fin):
    if debut >= fin:
        return

    pivot = tableau[debut]
    gauche = debut
    droite = fin - 1

    while gauche < droite:
        while gauche < fin and tableau[gauche] <= pivot:
            gauche += 1
        while droite >= gauche and tableau[droite] > pivot:
            droite -= 1
        if gauche < droite:
            tableau[gauche], tableau[droite] = tableau[droite], tableau[gauche]

    tableau[debut], tableau[droite] = tableau[droite], tableau[debut]
    # Appels récursifs
    tri_rapide_hoare(tableau, debut, droite)
    tri_rapide_hoare(tableau, droite + 1, fin)
# --- PYODIDE:tests --- #
tableau_0 = [3, 1, 2]
tri_rapide_hoare(tableau_0, 0, len(tableau_0))
assert tableau_0 == [1, 2, 3], "Erreur avec [3, 1, 2]"

# --- PYODIDE:secrets --- #
tableau_1 = [1, 2, 3, 4]
tri_rapide_hoare(tableau_1, 0, len(tableau_1))
assert tableau_1 == [1, 2, 3, 4], "Erreur avec [1, 2, 3, 4]"

tableau_2 = [-2, -5]
tri_rapide_hoare(tableau_2, 0, len(tableau_2))
assert tableau_2 == [-5, -2], "Erreur avec des valeurs négatives"

tableau_3 = []
tri_rapide_hoare(tableau_3, 0, len(tableau_3))
assert tableau_3 == [], "Erreur avec un tableau vide"
