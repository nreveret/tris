# --- PYODIDE:code --- #
def sous_tableau(tableau, debut, fin):
    nouveau = [None] * (fin - debut)  # pas indispensable
    ...


# --- PYODIDE:corr --- #
def sous_tableau(tableau, debut, fin):
    nouveau = [None] * (fin - debut)
    for i in range(debut, fin):
        nouveau[i - debut] = tableau[i]
    return nouveau
# --- PYODIDE:tests --- #
tableau = [8, 9, 10, 11, 12, 13]
assert sous_tableau(tableau, 0, 2) == [8, 9]
# --- PYODIDE:secrets --- #
tableau = [8, 9, 10, 11, 12, 13]
assert sous_tableau(tableau, 2, 6) == [10, 11, 12, 13]
assert sous_tableau(tableau, 0, 6) == [8, 9, 10, 11, 12, 13]
assert sous_tableau(tableau, 0, 0) == []
