On préférera néanmoins une version plus concise de cette fonction qui utilise les listes par compréhension :

```python
def sous_tableau(tableau, debut, fin):
    """
    Renvoie une copie des éléments d'indices
    compris entre debut (inclus) et fin (exclu)
    de tableau
    """
    return [tableau[valeur] for valeur in range(debut, fin)]
```

Enfin, Python offre la possibilité d'extraire des sous-tableaux à l'aide des *copies de tranches* : `#!py tableau[a:b]` renvoie ainsi une nouvelle liste constituée des éléments d'indices `a` (inclus) à `b` (exclu) de `#!py tableau`.

La fonction précédente peut donc aussi s'écrire :

```python
def sous_tableau(tableau, debut, fin):
    """
    Renvoie une copie des éléments d'indices
    compris entre debut (inclus) et fin (exclu)
    de tableau
    """
    return tableau[debut:fin]
```