# --- PYODIDE:code --- #
def maximum(tableau):
    ...


def tri_denombrement(entiers):
    ...


# --- PYODIDE:corr --- #
def maximum(tableau):
    maxi = 0
    for x in tableau:
        if x > maxi:
            maxi = x
    return maxi


def tri_denombrement(entiers):
    maxi = maximum(entiers)

    effectifs = [0] * (maxi + 1)

    for x in entiers:
        effectifs[x] += 1

    i = 0
    for x in range(maxi + 1):
        for _ in range(effectifs[x]):
            entiers[i] = x
            i += 1
# --- PYODIDE:tests --- #
nombres = [4, 5, 4, 2]
tri_denombrement(nombres)
assert nombres == [2, 4, 4, 5], "Erreur avec [4, 5, 4, 2]"

# --- PYODIDE:secrets --- #
nombres = [3, 8, 7, 3, 5]
tri_denombrement(nombres)
assert nombres == [3, 3, 5, 7, 8], "Erreur avec [3, 8, 7, 3, 5] "

nombres = [1, 2, 3, 4, 5]
tri_denombrement(nombres)
assert nombres == [1, 2, 3, 4, 5], "Erreur avec [1, 2, 3, 4, 5]"

nombres = [5, 4, 3, 2, 1]
tri_denombrement(nombres)
assert nombres == [1, 2, 3, 4, 5], "Erreur avec [5, 4, 3, 2, 1] "
