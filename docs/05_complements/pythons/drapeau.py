# --- PYODIDE:code --- #
def tri_drapeau(tableau, a, b, c):
    prochain_a = 0
    actuel = 0
    prochain_c = len(tableau) - 1

    while ... <= ...:
        if tableau[...] == a:
            tableau[...], tableau[...] = tableau[...], tableau[...]
            prochain_a = ...
            actuel = ...
        elif tableau[...] == b:
            actuel = ...
        else:
            tableau[...], tableau[...] = tableau[...], tableau[...]
            prochain_c =...


# --- PYODIDE:corr --- #
def tri_drapeau(tableau, a, b, c):
    prochain_a = 0
    actuel = 0
    prochain_c = len(tableau) - 1

    while actuel <= prochain_c:
        if tableau[actuel] == a:
            tableau[prochain_a], tableau[actuel] = tableau[actuel], tableau[prochain_a]
            prochain_a = prochain_a + 1
            actuel = actuel + 1
        elif tableau[actuel] == b:
            actuel = actuel + 1
        else:
            tableau[prochain_c], tableau[actuel] = tableau[actuel], tableau[prochain_c]
            prochain_c = prochain_c - 1
# --- PYODIDE:tests --- #
entiers = [2, 0, 2, 1, 2, 1]
a, b, c = 0, 1, 2
tri_drapeau(entiers, a, b, c)
assert entiers == [0, 1, 1, 2, 2, 2]

# --- PYODIDE:secrets --- #
neveux = ["Fifi", "Loulou", "Riri", "Riri", "Fifi"]
a, b, c = "Riri", "Fifi", "Loulou"
tri_drapeau(neveux, a, b, c)
assert neveux == ["Riri", "Riri", "Fifi", "Fifi", "Loulou"]
