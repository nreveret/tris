---
author: Nicolas Revéret
title: Crédits
---

Ce site a été réalisé par N. Revéret avec l'aide des membre du groupe *e-nsi*, en particulier M. Coilhac et F. Chambon. Les fichiers sources sont disponibles sur ce [dépôt](https://forge.apps.education.fr/nreveret/tris).

L'ensemble des documents sont sous licence [CC-BY-NC-SA 4.0 (Attribution, Utilisation Non Commerciale, ShareAlike)](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Le site est hébergé par la [Forge des communs numériques éducatifs](https://forge.apps.education.fr).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

Les logos ont été créés par [Andrean Prabowo - Flaticon](https://www.flaticon.com/free-icons/descending).

!!! tip "Merci !"

    Un grand merci à Frédéric Zinelli pour le développement de [`pyodide-mkdocs-theme`](https://frederic-zinelli.gitlab.io/pyodide-mkdocs-theme/).
